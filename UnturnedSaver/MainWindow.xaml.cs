﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace UnturnedSaver
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private RegEditSave res;

        public static RoutedCommand Save = new RoutedCommand();
        public static RoutedCommand New = new RoutedCommand();
        public static RoutedCommand Delete = new RoutedCommand();

        public MainWindow()
        {
            InitializeComponent();

            res = new RegEditSave(Root.CurrentUser, "Software\\Smartly Dressed Games\\Unturned");

            // Initialize Save information and focus the saves
            lbSaves.Focus();
            LoadSaves();
            LoadCurrentSave();

            // Add key binds
            Save.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            New.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            Delete.InputGestures.Add(new KeyGesture(Key.D, ModifierKeys.Control));

            // Register command bindings
            CommandBindings.Add(new CommandBinding(Save, bSave_Click));
            CommandBindings.Add(new CommandBinding(New, bNew_Click));
            CommandBindings.Add(new CommandBinding(Delete, bDelete_Click));
        }

        private bool NameExists(string name)
        {
            foreach (string str in res.GetSaves())
            {
                if (name == str)
                {
                    return true;
                }
            }

            return false;
        }

        private void LoadSaves()
        {
            lbSaves.Items.Clear();

            foreach (string save in res.GetSaves())
            {
                lbSaves.Items.Add(save);
            }

            lbSaves.SelectedIndex = 0;
        }

        private void LoadCurrentSave()
        {
            Save save = res.GetSave((string) lbSaves.SelectedValue);
            Save currentSave = res.GetCurrentSave();

            tbCurrent.Text = currentSave.Name;
            tbDate.Text = save.DateAndTime;
        }

        private void bLoad_Click(object sender, RoutedEventArgs e)
        {
            WindowConfirmation wConfirmation = new WindowConfirmation("Are you sure you want to load \"" + (string)lbSaves.SelectedValue + "\"?");

            if (wConfirmation.ShowDialog().Value == true)
            {
                res.Load((string)lbSaves.SelectedValue);

                LoadCurrentSave();
            }
        }

        private void bSave_Click(object sender, RoutedEventArgs e)
        {
            WindowInput wSave = new WindowInput("Save");

            wSave.InputEntered += new EventHandler<InputEventArgs>(wSave_DialogFinished);
            wSave.ShowDialog();
        }

        void wSave_DialogFinished(object sender, InputEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Input)) return;

            if (NameExists(e.Input))
            {
                WindowConfirmation wConfirmation = new WindowConfirmation("Would you like to overwrite this save?");

                if (wConfirmation.ShowDialog().Value == false)
                {
                    return;
                }
            }

            res.Save(e.Input);

            LoadSaves();
            LoadCurrentSave();
        }

        private void bNew_Click(object sender, RoutedEventArgs e)
        {
            WindowInput wInput = new WindowInput("New");

            wInput.InputEntered +=new EventHandler<InputEventArgs>(wInput_InputEntered);
            wInput.ShowDialog();
        }

        void wInput_InputEntered(object sender, InputEventArgs e)
        {
            if (String.IsNullOrEmpty(e.Input)) return;

            if (NameExists(e.Input))
            {
                WindowConfirmation wConfirmation = new WindowConfirmation("A save by that name already exists, overwrite?");

                if (wConfirmation.ShowDialog().Value == false)
                {
                    return;
                }
            }

            res.New(e.Input);
            res.Save(e.Input);

            LoadSaves();
            LoadCurrentSave();
        }

        private void bDelete_Click(object sender, RoutedEventArgs e)
        {
            WindowConfirmation wConfirmation = new WindowConfirmation("Are you sure you wish to delete save \"" + (string) lbSaves.SelectedValue + "\"?");

            if (wConfirmation.ShowDialog().Value == true)
            {
                res.Delete((string)lbSaves.SelectedValue);

                LoadSaves();
                LoadCurrentSave();
            }
        }

        private void lbSaves_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            LoadCurrentSave();
        }

        private void lbSaves_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                bLoad_Click(sender, e);
            }
        }

        private void miAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutBox ab = new AboutBox();

            ab.ShowDialog();
        }
    }
}
