﻿using System;
using System.Windows;

namespace UnturnedSaver
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class WindowInput : Window
    {
        public event EventHandler<InputEventArgs> InputEntered;

        public WindowInput()
        {
            InitializeComponent();
        }

        public WindowInput(string name)
        {
            InitializeComponent();

            this.Title = name;
        }

        private void bAccept_Click(object sender, RoutedEventArgs e)
        {
            if (InputEntered != null)
            {
                InputEntered(this, new InputEventArgs(tbSaveName.Text));
            }

            this.Close();
        }
    }

    public class InputEventArgs : EventArgs
    {
        public string Input { get; set; }

        public InputEventArgs(string name)
        {
            Input = name;
        }
    }
}
