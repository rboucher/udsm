﻿using System.Windows;

namespace UnturnedSaver
{
    /// <summary>
    /// Interaction logic for Confirmation.xaml
    /// </summary>
    public partial class WindowConfirmation : Window
    {
        public WindowConfirmation()
        {
            InitializeComponent();
        }

        public WindowConfirmation(string message)
        {
            InitializeComponent();

            tbMessage.Text = message;
        }

        private void bYes_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            this.Close();
        }

        private void bNo_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            this.Close();
        }
    }
}
