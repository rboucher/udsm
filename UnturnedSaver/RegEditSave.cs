﻿using System;
using System.Collections.Generic;
using Microsoft.Win32;

namespace UnturnedSaver
{
    public enum Root
    {
        CurrentUser,
        LocalMachine,
    }

    public class RegEditSave
    {
        private const string LAST_SAVE = "last_save_{0}";
        private const string CURRENT_SAVE = "current_save_name";

        private RegistryKey _key;
        private string _path;

        public RegEditSave(Root root, string path)
        {
            _path = path;

            switch (root)
            {
                case Root.CurrentUser:
                    _key = Registry.CurrentUser.OpenSubKey(path, true);
                    break;

                case Root.LocalMachine:
                    _key = Registry.LocalMachine.OpenSubKey(path, true);
                    break;
            }
        }

        /// <summary>
        /// Retrieves the current names of the subkeys residing under the Saves key
        /// </summary>
        /// <returns>A list of names representing the available saves</returns>
        public string[] GetSaves()
        {
            try
            {
                RegistryKey saves = _key.OpenSubKey("Saves", RegistryKeyPermissionCheck.ReadSubTree);

                return saves.GetSubKeyNames();
            }
            catch (Exception)
            {
                return new string[] {};
            }
        }

        /// <summary>
        /// Retrieves save information based on the name provided
        /// </summary>
        /// <param name="name">Name of the save to get</param>
        /// <returns>A save object containing the date, time, and name of the save</returns>
        public Save GetSave(string name)
        {
            try
            {
                RegistryKey saves = _key.OpenSubKey("Saves");

                string date = (string)saves.GetValue(string.Format(LAST_SAVE, name));

                return new Save(date, name);
            }
            catch (Exception)
            {
                return new Save("Unknown", "Unknown");
            }
        }

        /// <summary>
        /// Retreives the currently loaded save.
        /// </summary>
        /// <returns>The currently loaded save object containing date, time, and name of the save</returns>
        public Save GetCurrentSave()
        {
            try
            {
                return GetSave((string)_key.OpenSubKey("Saves").GetValue(CURRENT_SAVE));
            }
            catch (Exception)
            {
                return new Save("Unknown", "Unknown");
            }
        }

        /// <summary>
        /// Creates a new blank save and loads it
        /// </summary>
        /// <param name="name">Name of the save to create</param>
        public void New(string name)
        {
            DeleteKeyValues(_key);

            SetCurrentSave(name);
        }

        /// <summary>
        /// Delete a save
        /// </summary>
        /// <param name="name">Name of the save to delete</param>
        /// <returns>true if successful, false if the delet fails</returns>
        public bool Delete(string name)
        {
            RegistryKey saves = _key.OpenSubKey("Saves", true);

            try
            {
                saves.DeleteSubKeyTree(name);
                saves.DeleteValue(string.Format(LAST_SAVE, name));
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Save the current data to a specific save
        /// </summary>
        /// <param name="name">Name of the save</param>
        public void Save(string name)
        {
            SaveKey(_key, name);
        }

        /// <summary>
        /// Loads a save
        /// </summary>
        /// <param name="name">Save to load</param>
        public void Load(string name)
        {
            RegistryKey specificSave = _key.OpenSubKey("Saves\\" + name);

            // Delete our root values
            DeleteKeyValues(_key);

            // Copy specific save to root
            CopyKeyValues(specificSave, _key);

            // Set the current save
            SetCurrentSave(name);
        }

        /// <summary>
        /// Updates which save is currently loaded
        /// </summary>
        /// <param name="name">Name of the save that is loaded</param>
        private void SetCurrentSave(string name)
        {
            RegistryKey saves = _key.OpenSubKey("Saves", true);

            saves.SetValue(CURRENT_SAVE, name, RegistryValueKind.String);
        }

        /// <summary>
        /// Save root data to Saves sub key
        /// </summary>
        /// <param name="key">Rootkey</param>
        /// <param name="name">Name of the save</param>
        private void SaveKey(RegistryKey key, string name)
        {
            RegistryKey saves = key.CreateSubKey("Saves", RegistryKeyPermissionCheck.ReadWriteSubTree);
            RegistryKey specificSave = saves.CreateSubKey(name, RegistryKeyPermissionCheck.ReadWriteSubTree);

            // Set our new save name
            SetCurrentSave(name);

            // Delete any previous data
            DeleteKeyValues(specificSave);

            // Copy new values in from primary
            CopyKeyValues(_key, specificSave);

            // Set our last save date
            saves.SetValue(string.Format(LAST_SAVE, name), DateTime.Now.ToString("f"), RegistryValueKind.String);
        }

        private string[] CopyKeyValues(RegistryKey toCopyKey, RegistryKey toAcceptKey)
        {
            List<string> invalidValues = new List<string>();

            foreach (string valueName in toCopyKey.GetValueNames())
            {
                try
                {
                    toAcceptKey.SetValue(valueName, toCopyKey.GetValue(valueName), toCopyKey.GetValueKind(valueName));
                }
                catch (Exception ex)
                {
                    invalidValues.Add(ex.Message);
                }
            }

            return invalidValues.ToArray();
        }

        private string[] DeleteKeyValues(RegistryKey key)
        {
            List<string> errors = new List<string>();

            foreach (string valueName in key.GetValueNames())
            {
                try
                {
                    key.DeleteValue(valueName);
                }
                catch (Exception ex)
                {
                    errors.Add(ex.Message);
                }
            }

            return errors.ToArray();
        }
    }
}
