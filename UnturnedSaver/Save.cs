﻿namespace UnturnedSaver
{
    public class Save
    {
        public string DateAndTime { get; set; }
        public string Name { get; set; }

        public Save(string dt, string name)
        {
            DateAndTime = dt ?? "Unknown";
            Name = name ?? "Unknown";
        }
    }
}
