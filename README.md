#Unturned Dynamic Save Manager

##Features
* Manage a dynamic number of saves
* Simple interface
* Save by name

##Usage
###Actions
* Save: Saves what is currently loaded in Unturned (or what is in the root directory)
* Load: Loads the selected listbox save
* New: Creates a blank new save
* Delete: Deletes the selected listbox save

###Keybinds
####Window
* Ctrl-D: Delete selected save
* Ctrl-S: Create a new save
* Ctrl-N: Create a blank new game

####Saves List
* Up: Traverse up in the saves list
* Down: Traverse down in the saves list
* Enter: Select save to load

##Installation
* No installation required, simply download the executable, or compile the program yourself.